/**
 * Created by Maks on 26.12.2017.
 */
let express = require('express');
let bodyParser = require('body-parser');
let app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

let artists = [
    {
        id: 1,
        name: 'Maks'
    },
    {
        id: 2,
        name: 'Ivan'
    },
    {
        id: 3,
        name: 'Taras'
    },
];


app.get('/', (req, res)=>
{
    res.send('hello world');
});

app.get('/artists', (req, res)=>
{
    res.send(artists);
});

app.get('/artist/:id', (req, res)=>
{
    console.log(req.params);
    let artist = artists.find((artist)=>
    {
        return artist.id === Number(req.params.id);
    });
    if(artist)
        res.send(artist);
    else
        res.send('Артист не найден!');
});

app.post('/artist', (req, res)=>
{
    let artist = {
        id: Date.now(),
        name: req.body.name
    };
    artists.push(artist);
    res.send(artists);
});

app.put('/artist/:id', (req, res)=>
{
    let artist = artists.find((artists)=>
    {
        return artists.id === Number(req.params.id)
    });
    artist.name = req.body.name;
    res.sendStatus(200);
});

app.delete('/artist/:id', (req, res)=>
{
    artists = artists.filter((artist)=>
    {
        return artist.id !== Number(req.params.id)
    });
    // res.sendStatus(200);
    res.send(artists);
});

app.listen(777, ()=>
{
    console.log('server start');
});